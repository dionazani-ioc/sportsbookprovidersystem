﻿using SportBook.Entity.Wallet;
using SportBook.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Service
{
    public abstract class Service
    {
        protected string partnerCode;
        protected string userName;

        protected void BreakPlayerCode(string playerCode)
        {
            string[] arrString = playerCode.Split("-");

            this.partnerCode = arrString[0];
            this.userName = arrString[1];
        }

        public bool IsPlayerExist(string partnerCode, string userName)
        {
            bool isPlayerExist = false;

            WalletRepository walletRepository = new WalletRepository();
            PlayerPartnerEntity userPartnerEntity = walletRepository.GetUserPartner(partnerCode, userName);

            isPlayerExist = userPartnerEntity != null ? true : false;

            return isPlayerExist;
        }
    }
}
