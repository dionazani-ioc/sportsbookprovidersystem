﻿
using AutoMapper;
using Microsoft.Extensions.Configuration;
using SportBook.Entity.Wallet;
using SportBook.Model.Balance;
using SportBook.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Service
{
    public class WalletService : Service
    {
        public Dictionary<string, object> GetPlayerBalance(string playerCode, 
                                                                string currency,
                                                                int deviceId,
                                                                bool isTest,
                                                                string command)
        {
            PlayerBalanceModel playerBalanceModel = null;
            Dictionary<string, object> map = new Dictionary<string, object>();

            this.BreakPlayerCode(playerCode);

            // check player is exists
            if (!this.IsPlayerExist(this.partnerCode, this.userName))
            {
                map.Add("success", "false");
                map.Add("status", "S001");
                map.Add("description", "Player does not exist");

                return map;
            }

            // if command is balance.
            if (command.ToLower().Equals("balance"))
                playerBalanceModel = this.GetPlayerAccountBalance(this.partnerCode, this.userName, currency);

            map.Add("success", "0");
            map.Add("contents", playerBalanceModel);

            return map;
        }

        private PlayerBalanceModel GetPlayerAccountBalance(string partnerCode,
                                                    string userName,
                                                    string currency)
        {
            PlayerBalanceModel playerBalanceModel = null;

            WalletRepository balanceRepository = new WalletRepository();
            PlayerAccountBalanceEntity userAccountBalanceEntity = balanceRepository.GetPlayerAccountBalance(partnerCode, userName, currency);

            if (userAccountBalanceEntity != null)
            {
                playerBalanceModel = new PlayerBalanceModel();
                playerBalanceModel.Balance = userAccountBalanceEntity.Balance;
                playerBalanceModel.CurrencyCode = userAccountBalanceEntity.CurrencyId;
            }

            return playerBalanceModel; 
        }

        public Dictionary<string, object> GetPlayerBalanceAfterBetting(string playerCode,
                                                            string productCode,
                                                            string currency,
                                                            string deviceId,
                                                            string isTest,
                                                            string command,
                                                            string trxId,
                                                            string roundId,
                                                            decimal amount,
                                                            decimal jpc)
        {
            PlayerBalanceModel playerBalanceModel = null;
            Dictionary<string, object> map = new Dictionary<string, object>();

            // break player code into partnerCode, userName.
            this.BreakPlayerCode(playerCode);

            WalletRepository walletRepository = new WalletRepository();
            JournalRepository journalRepository = new JournalRepository();

            PlayerAccountBalanceEntity playerAccountBalanceEntity = walletRepository.GetPlayerAccountBalance(partnerCode, userName, currency);

            // check userAccountBalanceEntity is null or not.
            if (playerAccountBalanceEntity == null)
            {
                map.Add("success", "false");
                map.Add("status", "1");
                map.Add("description", "Failed during executed, userAccountBalanceEntity is null");

                return map;
            }

            // check insufficient funds.
            if (playerAccountBalanceEntity.Balance < amount)
            {
                map.Add("success", "false");
                map.Add("status", "S001");
                map.Add("description", "Insufficient Funds.");

                return map;
            }

            // if command is bet.
            if (command.ToLower().Equals("bet"))
            {
                decimal balance = playerAccountBalanceEntity.Balance - amount;
                int updateUserAccount = walletRepository.UpdatePlayerAccountBalance(playerAccountBalanceEntity.UserId, playerAccountBalanceEntity.CurrencyId, balance);
                int addJournal = journalRepository.AddJournal(playerAccountBalanceEntity.UserId, playerAccountBalanceEntity.CurrencyId, "PKC-001", amount, "EXTTRXID");
                playerBalanceModel = this.GetPlayerAccountBalance(this.partnerCode, this.userName, currency);
            }

            map.Add("success", "true");
            map.Add("status", "0");
            map.Add("contents", playerBalanceModel);

            return map;
        }
    }
}
