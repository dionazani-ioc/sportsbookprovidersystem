﻿using SportBook.Entity;
using SportBook.Entity.Transfer;
using SportBook.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Service
{
    public class WithdrawService : Service
    {
        public TransferEntity Withdraw(string playerCode, string currency, int deviceId, string isTest, string command, string providerCode, string transferToProduct, int amount, string postingKeyCode, string trxId)
        {
            TransferEntity transferDepositEntity = null;
            this.BreakPlayerCode(playerCode);

            if (this.IsPlayerExist(this.partnerCode, this.userName))
            {
                if (command.ToLower().Equals("deposit"))
                {
                    transferDepositEntity = this.DoCommandWithdraw(playerCode, this.partnerCode, this.userName, currency, deviceId, isTest, command, providerCode, transferToProduct, amount, postingKeyCode, trxId);
                }
            }
            else
            {
                throw new Exception("Player does not exist");
            }

            return transferDepositEntity;
        }

        private TransferEntity DoCommandWithdraw(string playerCode, string partnerCode, string userName, string currency, int deviceId, string isTest, string command, string providerCode, string transferToProduct, int amount, string postingKeyCode, string trxId)
        {
            TransferEntity transferDepositEntity = null;
            int userAccountHistory = 0;
            PlayerAccountEntity balanceBefore = null;
            PlayerAccountEntity balanceAfter = null;
            PlayerAccountRepository userRepository = new PlayerAccountRepository();
            PlayerEntity userEntity = userRepository.GetPlayerByUsername(userName);
            TransferRepository depositRepository = new TransferRepository();

            if (depositRepository.CheckingBalance(userEntity.Id, amount))
            {
                balanceBefore = depositRepository.GetBalanceActive(userEntity.Id);
                var totalBalance = balanceBefore.Balance - amount;
                int deposit = depositRepository.UpdateBalance(userEntity.Id, currency, totalBalance);
                if (deposit > 0)
                {
                    PlayerAccountRepository userAccountRepository = new PlayerAccountRepository();
                    PlayerAccountEntity userAccountEntity = userAccountRepository.GetPlayerAccountEntityByUserId(userEntity.Id);
                    JournalRepository jopurnalRepository = new JournalRepository();
                    int journalId = jopurnalRepository.AddJournal(userEntity.Id, currency, postingKeyCode, amount, trxId);
                    if (journalId > 0)
                    {
                        balanceAfter = depositRepository.GetBalanceActive(userEntity.Id);

                        PlayerAccountHistoryRepository userAccountHistoryRepository = new PlayerAccountHistoryRepository();
                        userAccountHistory = userAccountHistoryRepository.AddHistory(userAccountEntity.Id, journalId, balanceBefore.Balance, balanceAfter.Balance);

                        if (userAccountHistory > 0)
                        {
                            transferDepositEntity = new TransferEntity();
                            transferDepositEntity.Status = "0";
                            transferDepositEntity.ExtTrxId = journalId.ToString();
                            transferDepositEntity.Amount = amount;
                            transferDepositEntity.Currency = currency;
                            transferDepositEntity.PlayerCode = playerCode;
                            transferDepositEntity.TrxId = trxId;
                            transferDepositEntity.TrxDate = DateTime.Now;
                        }
                    }
                }
            }
            if (transferDepositEntity != null)
            {
                return transferDepositEntity;
            }
            else
            {
                throw new Exception("Data not found");
            }
        }
    }
}
