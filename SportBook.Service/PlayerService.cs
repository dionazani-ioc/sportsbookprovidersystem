﻿using AutoMapper;
using SportBook.Entity;
using SportBook.Model.Player;
using SportBook.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Service
{
    public class PlayerService : Service
    {
        public Dictionary<string, object> SavePlayer(PlayerModel playerModel)
        {
            Dictionary<string, object> map = new Dictionary<string, object>();
            int saved = 0;

            if (String.IsNullOrEmpty(playerModel.PlayerName))
            {
                map.Add("success", "false");
                map.Add("description", "Player cannot blank");
            }
                
            if (playerModel.Id == 0)
                saved = this.AddPlayer(playerModel);

            if (saved > 0)
            {
                map.Add("success", "true");
                map.Add("description", "");
            }
            else
            {
                map.Add("success", "false");
                map.Add("description", "Cannot add new Player");
            }

            return map;
        }

        private int AddPlayer(PlayerModel playerModel)
        {
            int saved = 0;

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<PlayerModel, PlayerEntity>();
            });

            IMapper iMapper = config.CreateMapper();
            PlayerEntity playerEntity = iMapper.Map<PlayerModel, PlayerEntity>(playerModel);

            PlayerRepository playerRepository = new PlayerRepository();
            saved = playerRepository.AddPlayer(playerEntity);

            return saved;

        }
    }
}
