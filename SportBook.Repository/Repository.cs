﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Repository
{
    public abstract class Repository
    {
        protected readonly IConfiguration configuration;

        public Repository()
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("AppSettings.json");
            this.configuration = configurationBuilder.Build();
        }

        public Repository(IConfiguration configuration)
        {
            this.configuration = configuration;
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            // Duplicate here any configuration sources you use.
            configurationBuilder.AddJsonFile("AppSettings.json");
            this.configuration = configurationBuilder.Build();
        }

        protected string DefaultConnectionString()
        {
            return configuration.GetConnectionString("DefaultConnection");
        }
    }
}
