
using Dapper;
using Npgsql;
using SportBook.Entity;
using SportBook.Entity.Transfer;
using SportBook.Entity.Wallet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SportBook.Repository
{
    public class TransferRepository : Repository
    {
        public bool CheckingBalance(int playerCode,int amount)
        {
            var sql = @"SELECT * FROM user_account WHERE user_id=@user_id AND wallet_type='active'";
            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                PlayerAccountEntity userAccountEntity = conn.Query<PlayerAccountEntity>(sql,new { user_id = playerCode}).SingleOrDefault();
                if (userAccountEntity.Balance >= amount)
                {
                    return true;
                }
            }
            return false;
        }

        public PlayerAccountEntity GetBalanceActive(int playerCode)
        {
            PlayerAccountEntity userAccountEntity = null;

            var sql = @"SELECT * FROM user_account WHERE user_id=@user_id AND wallet_type='active'";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                userAccountEntity = conn.Query<PlayerAccountEntity>(sql,new { user_id = playerCode}).SingleOrDefault();
            }

            return userAccountEntity;
        }

        public int UpdateBalance(int playerCode,string currencySysmbol,decimal amount)
        {
            int userAccountEntity = 0;
            
            var sql = @"UPDATE user_account SET balance=@balance WHERE user_id=@user_id AND wallet_type='active'";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
               conn.Open();
               userAccountEntity = conn.Execute(sql,new { balance = amount,user_id = playerCode });
            }

            return userAccountEntity;
        }
    }
}

