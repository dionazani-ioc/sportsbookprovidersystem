﻿using Dapper;
using Npgsql;
using SportBook.Entity.Wallet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SportBook.Repository
{
    public class JournalRepository : Repository
    {
        public int AddJournal(int userId, string currencyId, string postingKeyCode, decimal amount, string extTrxId)
        {
            int journalId = 0;

            var sql = @"INSERT INTO journal(user_id,currency_id, posting_key_code,amount,ext_trx_id,status) 
                        VALUES(@user_id, @currency_id, @posting_key_id, @amount, @ext_trx_id, 'active')
                        RETURNING id";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                Object idReturn = conn.ExecuteScalar(sql, new { user_id = userId, currency_id = currencyId, posting_key_id = postingKeyCode, amount = amount, ext_trx_id = extTrxId});
                journalId = Convert.ToInt32(idReturn);
            }

            return journalId;
        }
    }
}