using Dapper;
using Npgsql;
using SportBook.Entity;
using SportBook.Entity.Transfer;
using SportBook.Entity.Wallet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SportBook.Repository
{
    public class DepositRepository : Repository
    {

        public TransferDepositEntity UserDeposit(string playerCode, string currency, int deviceId, string isTest, string command, string providerCode, string transferToProduct, int amount, string postingKeyCode,string trxId)
        {
            TransferDepositEntity transferDepositEntity = null;
            int userAccountHistory = 0;
            UserAccountEntity balanceBefore = null;
            UserAccountEntity balanceAfter = null;

            if (this.CheckingBalance(playerCode, amount))
            {
                balanceBefore = this.GetBalanceActive(playerCode);
                UserAccountEntity deposit = this.DepositBalance(playerCode, currency, amount);
                if (deposit != null)
                {
                    JournalRepository jopurnalRepository = new JournalRepository();
                    int journalId = jopurnalRepository.AddJournal(deposit.UserId,currency,postingKeyCode,amount,trxId);

                    balanceAfter = this.GetBalanceActive(playerCode);

                    UserAccountHistoryRepository userAccountHistoryRepository = new UserAccountHistoryRepository();
                    userAccountHistory = userAccountHistoryRepository.AddHistory(deposit.Id, journalId, balanceBefore.Balance,balanceAfter.Balance,"active");

                    if(userAccountHistory > 0)
                    {
                        transferDepositEntity.Status = "0";
                        transferDepositEntity.ExtTrxId = journalId;
                        transferDepositEntity.Amount = amount;
                        transferDepositEntity.Currency = currency;
                        transferDepositEntity.playerCode = playerCode;
                        transferDepositEntity.TrxId = trxId;
                        transferDepositEntity.TrxDate = DateTime.Now;
                    }
                }
            }
            return transferDepositEntity;
        }

        private bool CheckingBalance(string playerCode,int amount)
        {
            var sql = @"SELECT balance FROM user_account WHERE user_id=@user_id";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                UserAccountEntity userAccountEntity = conn.Query(sql,new { user_id = playerCode}).SingleOrDefault();
                if (userAccountEntity.Balance >= amount)
                {
                    return true;
                }
            }
            return false;
        }

        private UserAccountEntity GetBalanceActive(string playerCode)
        {
            UserAccountEntity userAccountEntity = null;

            var sql = @"SELECT user_id,balance,currency_id FROM user_account WHERE user_id=@user_id AND status=@status";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                userAccountEntity = conn.Query(sql,new { user_id = playerCode,status="active"}).SingleOrDefault();
            }

            return userAccountEntity;
        }

        private UserAccountEntity DepositBalance(string playerCode,string currencySysmbol,decimal amount)
        {
            WalletRepository walletRepository = new WalletRepository();
            UserAccountEntity userAccountEntity = null;
            UserAccountBalanceEntity userAccountBalanceEntity = walletRepository.GetUserAccountBalance(playerCode,currencySysmbol);
            var totalBalance = userAccountBalanceEntity.Balance + amount;
            var sql = @"UPDATE user_account SET balance=@balance WHERE user_id=@user_id";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
               conn.Open();
               userAccountEntity = conn.Query(sql,new { balance = totalBalance,user_id = UserAccountBalanceEntity.UserId }).SingleOrDefault();
            }

            return userAccountEntity;
        }
    }
}

