﻿using Dapper;
using Npgsql;
using SportBook.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SportBook.Repository
{
    public class PlayerAccountRepository : Repository
    {
        public int AddPlayerAccount(PlayerAccountEntity playerAccountEntity)
        {
            int userAcountId = 0;

            var sql = @"INSERT INTO partner.player_account (user_id, currency_id, wallet_type, balance, status)
                            VALUES(@user_id, @currency_id, @wallet_type, @balance, 'active')
                        RETURN id";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                Object idReturn = conn.ExecuteScalar(sql, new { user_id = playerAccountEntity.UserId,
                                                currency_id = playerAccountEntity.CurrencyId,
                                                posting_key_id = playerAccountEntity.WalletType,
                                                balance = playerAccountEntity.Balance });
                userAcountId = Convert.ToInt32(idReturn);

            }

            return userAcountId;
        }

        public PlayerAccountEntity GetPlayerAccountById (int id)
        {
            PlayerAccountEntity userAccountEntity = null;

            var sql = @"SELECT id, player_id, currency_id, wallet_type, balance, creation_time, last_update_time, status
                        FROM partner.player_account";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                userAccountEntity = conn.Query(sql, new { id = id }).SingleOrDefault();

            }

            return userAccountEntity;
        }

        public int UpdatePlayerAccountBalance(int userId, decimal balanceAmount)
        {
            int updated = 0;
            var sql = @"UPDATE partner.player_account SET balance=@balance WHERE user_id=@user_id";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                updated = conn.Execute(sql, new { balance = balanceAmount, user_id = userId });
            }

            return updated;
        }

        public PlayerEntity GetPlayerByUsername(string userName)
        {
            PlayerEntity userEntity = null;

            var sql = @"SELECT * FROM partner.player WHERE partner.player.user_name=@user_name";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                userEntity = conn.Query<PlayerEntity>(sql, new { user_name = userName }).SingleOrDefault();
            }
            return userEntity;
        }

        public PlayerAccountEntity GetPlayerAccountEntityByUserId(int userId)
        {
            PlayerAccountEntity userAccountEntity = null;

            var sql = @"SELECT id, player_id, currency_id, wallet_type, balance, creation_time, last_update_time, status
                        FROM partner.player_account WHERE user_id=@user_id AND wallet_type='active'";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                userAccountEntity = conn.Query<PlayerAccountEntity>(sql, new { user_id = userId }).SingleOrDefault();

            }

            return userAccountEntity;
        }
    }
}
