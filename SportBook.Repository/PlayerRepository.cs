﻿using Dapper;
using Npgsql;
using SportBook.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SportBook.Repository
{
    public class PlayerRepository : Repository
    {
        public int AddPlayer(PlayerEntity playerEntity)
        {
            int playerId = 0;

            var sql = @"INSERT INTO partner.player
                        (partner_id, user_name, status, player_classification)
                        VALUES(@partner_id, @user_name, 'active', 'reguler')
                        RETURN id";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                Object idReturn = conn.ExecuteScalar(sql, new
                {
                    partner_id = playerEntity.PartnerId,
                    user_name = playerEntity.PlayerName
                });

                playerId = Convert.ToInt32(idReturn);

            }

            return playerId;
        }
    }
}
