using Dapper;
using Npgsql;
using SportBook.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SportBook.Repository
{
    public class PlayerAccountHistoryRepository : Repository
    {
        public int AddHistory(int userAccountId,int journalId,decimal balanceBefore,decimal balanceAfter)
        {
            int added = 0;
            var sql = @"INSERT INTO user_account_history(user_account_id,journal_id,balance_before,balance_after,status) 
                        VALUES(@user_account_id,@journal_id,@balance_before,@balance_after,'active')";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                added = conn.Execute(sql,new { user_account_id = userAccountId, journal_id = journalId, balance_before = balanceBefore, balance_after = balanceAfter });
            }
            return added;
        }
    }
}
