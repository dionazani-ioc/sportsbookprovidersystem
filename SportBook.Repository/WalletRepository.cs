﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Data;
using System.Linq;
using System;
using SportBook.Entity.Wallet;

namespace SportBook.Repository
{
    public class WalletRepository : Repository
    {
        public PlayerPartnerEntity GetUserPartner(string partnerCode, string userName)
        {
            PlayerPartnerEntity userPartnerEntity = null;
            string sql = @"SELECT partner.player.id as user_id,
                            partner.player.partner_id,
                            partner.partner.partner_code
                            FROM partner.player inner join partner ON partner.player.partner_id = partner.partner.id
                            WHERE partner.player.user_name = @user_name 
                                AND partner.partner.partner_code = @partner_code";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                userPartnerEntity = conn.Query<PlayerPartnerEntity>(sql, new {user_name = userName,
                                                                partner_code = partnerCode}).SingleOrDefault();
            }

            return userPartnerEntity;
        }

        public PlayerAccountBalanceEntity GetPlayerAccountBalance(string partnerCode, string userName, string currency)
        {
            PlayerAccountBalanceEntity userAccountBalanceEntity = null;

            string sql = @"SELECT partner.player.id as user_id,
                                partner.player_account.id as user_account_id,
                                partner.player_account.balance,
                                partner.currencies.id as currency_id
                        FROM partner.partner INNER JOIN partner.player ON partner.player.partner_id = partner.partner.id
		                        INNER JOIN partner.player_account ON partner.player_account.user_id = partner.player.id
      	                        INNER JOIN partner.currencies on partner.currencies.id = partner.player_account.currency_id
                        WHERE partner.partner_code = @partner_code
                            AND partner.player.user_name = @user_name 
                            AND partner.currencies.id = @currency_symbol
                            AND partner.player_account.wallet_type = 'active'";

            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                userAccountBalanceEntity = conn.Query<PlayerAccountBalanceEntity>(sql, new {partner_code = partnerCode,
                                                                                            user_name = userName,
                                                                                            currency_symbol = currency
                }).SingleOrDefault();
            }

            return userAccountBalanceEntity;
        }

        public int UpdatePlayerAccountBalance(int userId, string currencyId, decimal balance)
        {
            int updated = 0;
            string sql = @"UPDATE partner.player_account SET balance = @balance 
                            WHERE partner.partner_account.user_id = @user_id 
                            AND partner.partner_account.currency_id = @currency_id";
            using (IDbConnection conn = new NpgsqlConnection(this.DefaultConnectionString()))
            {
                conn.Open();
                updated = conn.Execute(sql, new { balance = balance, user_id = userId, currency_id = currencyId });

            }

            return updated;
        }

    }
}
