﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Entity.Wallet
{
    public class PlayerPartnerEntity
    {
        public int UserId { get; set; }
        public int PartnerId { get; set; }
        public string PartnerCode { get; set; }
    }
}
