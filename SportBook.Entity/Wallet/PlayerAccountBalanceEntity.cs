﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Entity.Wallet
{
    public class PlayerAccountBalanceEntity
    {
        public int UserId { get; set; }
        public int UserAccount { get; set; }
        public decimal Balance { get; set; }
        public string CurrencyId { get; set; }
    }
}
