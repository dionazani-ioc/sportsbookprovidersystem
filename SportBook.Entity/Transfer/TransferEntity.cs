﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Entity.Transfer
{
    public class TransferEntity
    {
        public string Status { set; get; }
        public string ExtTrxId { set; get; }
        public string PlayerCode { set; get; }
        public string Currency { set; get; }
        public DateTime TrxDate { set; get; }
        public decimal Amount { set; get; }
        public string TrxId { set; get; }
    }
}
