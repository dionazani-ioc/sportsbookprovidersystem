﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Entity
{
    public class PlayerEntity
    {
        public int Id { get; set; }
        public int PartnerId { get; set; }
        public string PlayerName {get; set;}
        public DateTime CreationTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public string PlayerClassification { get; set; }
    }
}
