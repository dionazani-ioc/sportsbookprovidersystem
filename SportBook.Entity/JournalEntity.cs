using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Entity.Wallet
{
    public class JournalEntity
    {
        public int UserId { set; get; }
        public string CurrencyId { set; get; }
        public string PostingKeyCode { set; get; }
        public int Amount { set; get; }
        public int ExtTrxId { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime LastUpdateTime { set; get; }
        public string status { set; get; }
    }
}
