﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportBook.Model.Player;
using SportBook.Service;

namespace SportBook.Web.Rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        [HttpPost("save")]
        public ActionResult SavePlayerAccount([FromBody] PlayerModel playerModel)
        {
            Dictionary<string, object> map = new Dictionary<string, object>();

            try
            {
                PlayerService playerService = new PlayerService();
                map = playerService.SavePlayer(playerModel);
            }
            catch (Exception ex)
            {
                map.Add("success", "false");
                map.Add("status", "1");
                map.Add("description", "Failed during executed, " + ex.Message);
            }

            return Ok(map);
        }
    }
}