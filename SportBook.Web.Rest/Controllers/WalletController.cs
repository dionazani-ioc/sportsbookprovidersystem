﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SportBook.Entity.Transfer;
using SportBook.Model.Balance;
using SportBook.Service;

namespace SportBook.Web.Rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WalletController : ControllerBase
    {
        [HttpGet("balance")]
        public ActionResult GetPlayerBalance(string playerCode, 
                                                    string productCode, 
                                                    string currency, 
                                                    int deviceId,
                                                    bool isTest,
                                                    string command)
        {
            Dictionary<string, object> map = new Dictionary<string, object>();

            try
            {
                WalletService balanceService = new WalletService();
                map = balanceService.GetPlayerBalance(playerCode, currency, deviceId, isTest, command);
            }
            catch(Exception ex)
            {
                map.Add("success", "false");
                map.Add("status", "1");
                map.Add("description", "Failed during executed, " + ex.Message);
            }

            return Ok(map);

        }

        [HttpGet("bet")]
        public ActionResult GetPlayerBalanceAfterBetting(string playerCode,
                                                            string productCode,
                                                            string currency,
                                                            int deviceId,
                                                            bool isTest,
                                                            string command,
                                                            string trxId,
                                                            string roundId,
                                                            decimal amount,
                                                            decimal jpc)
        {
            Dictionary<string, object> map = null;

            try
            {
                WalletService walletService = new WalletService();
                map = walletService.GetPlayerBalance(playerCode, currency, deviceId, isTest, command);
            }
            catch (Exception ex)
            {
                map.Add("status", "1");
                map.Add("description", "Failed during exeption, " + ex.Message);
                map.Add("contents", null);
            }

            return Ok(map);
        }

        [HttpPost("Transfer/Deposit")]
        public ActionResult Deposit(string playerCode, string currency, int deviceId, string isTest, string command, string providerCode, string transferToProduct, int amount, string postingKeyCode, string trxId)
        {
            Dictionary<string, object> map = new Dictionary<string, object>();

            try
            {
                DepositService depositService = new DepositService();

                // Normal.
                TransferEntity transferDepositEntity = depositService.Deposit(playerCode, currency, deviceId, isTest, command, providerCode, transferToProduct, amount, postingKeyCode, trxId);

                map.Add("success", "true");
                map.Add("contents", transferDepositEntity);
            }
            catch (Exception ex)
            {
                map.Add("success", "false");
                map.Add("status", "1");
                map.Add("message", ex.Message);
            }

            return Ok(map);

        }

        [HttpPost("Transfer/Withdraw")]
        public ActionResult Withdraw(string playerCode, string currency, int deviceId, string isTest, string command, string providerCode, string transferToProduct, int amount, string postingKeyCode, string trxId)
        {
            Dictionary<string, object> map = new Dictionary<string, object>();

            try
            {
                WithdrawService withdrawService = new WithdrawService();

                // Normal.
                TransferEntity transferDepositEntity = withdrawService.Withdraw(playerCode, currency, deviceId, isTest, command, providerCode, transferToProduct, amount, postingKeyCode, trxId);

                map.Add("success", "true");
                map.Add("contents", transferDepositEntity);
            }
            catch (Exception ex)
            {
                map.Add("success", "false");
                map.Add("status", "1");
                map.Add("message", ex.Message);
            }

            return Ok(map);

        }
    }
}