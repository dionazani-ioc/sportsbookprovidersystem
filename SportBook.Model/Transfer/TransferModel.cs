using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Model.Wallet
{
    public class TransferModel
    {
        public string PlayerCode { set; get; }
        public string Currency { set; get; }
        public int DeviceId { set; get; }
        public bool IsTest { set; get; }
        public string Command { set; get; }
        public string ProviderCode { set; get; }
        public string TransferToProduct { set; get; }
        public decimal Amount { set; get; }
        public string PostingKeyCOde { set; get; }
    }
}
