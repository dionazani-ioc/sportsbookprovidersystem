using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Model.Wallet
{
    public class TransferSetBettingModel : TransferModel
    {
        public Guid SessionId { set; get; }
        public string ProductCode { set; get; }
        public string RoundId { set; get; }
        public decimal Jpc { set; get; }
    }
}
