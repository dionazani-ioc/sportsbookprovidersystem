using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Model.Wallet
{
    public class TransferResultModel
    {
        public string ExtTrxId { set; get; }
        public string Currency { set; get; }
        public DateTime TrxDate { set; get; }
        public decimal Amount { set; get; }
        public string TrxId { set; get; }
    }
}
