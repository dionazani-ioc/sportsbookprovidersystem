﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Model.Balance
{
    public class PlayerModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public string Status { get; set; }
        public int Salt { get; set; }
        public string email_address { get; set; }
        public DateTime Birthday { get; set; }
        public string UserType { get; set; }
        public string ReferralCode { get; set; }
        public string UserCode { get; set; }
        public int PartnerId { get; set; }
    }
}
