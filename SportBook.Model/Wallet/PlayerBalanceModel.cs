﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Model.Balance
{
    public class PlayerBalanceModel
    {
        public decimal? Balance { get; set; }
        public string CurrencyCode { get; set; }
    }
}
