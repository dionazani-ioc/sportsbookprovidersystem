﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportBook.Model.Player
{
    public class PlayerAccountModel
    {
        public int Id { get; set; }
        public int UserId { set; get; }
        public string CurrencyId { set; get; }
        public string WalletType { set; get; }
        public decimal Balance { set; get; }
        public string Status { set; get; }
    }
}
